@include('layouts.header', ['title' => 'Картка товару: ' . $drug->title ])
@include('layouts.nav')
<?php
    function capitalize($word){
        $firstLetter = substr($word, 0, 1);
        $restOfWord  = substr($word, 1);

        $firstLetter = strtoupper($firstLetter);
        $restOfWord  = strtolower($restOfWord);
        return $firstLetter .$restOfWord;
    }
?>
<div class="content product__card">
    <div class="container">
        @include('layouts.aside_nav')
        <div class="content__productCard__breadcrumb">
            <ul>
                <li><a href="{{ route('pages.index') }}">Головна</a></li>
                @if($drug->atsCategory)
                    <li><span>&nbsp;/&nbsp;</span></li>
                    <li><a href="{{ route('sub-classification.show', $drug->atsCategory->id) }}">
                            @if( strlen($drug->atsCategory->title) > 30) {{  capitalize( mb_substr($drug->atsCategory->title,0,  30,  'UTF-8') . "...") }} @else {{ capitalize($drug->atsCategory->title) }} @endif
                        </a></li>
                @endif
                <li><span>&nbsp;/&nbsp;</span></li>
                <li><span>{{ $drug->title }}</span></li>
            </ul>

            <h5>{{ $drug->title }}</h5>
        </div>
        <div class="content__productCard__img">
            <ul id="slideshow" class="slideshow">
                @if(count($drug->images))
                    @foreach($drug->images as $image)
                        <li><img src="{{ asset($image->path) }}" alt=""></li>
                    @endforeach
                @endif
            </ul>
            <ul id="slideshowNav" class="slideshowNav">
                @if(count($drug->images))
                    @foreach($drug->images as $image)
                        <li><img src="{{ asset($image->path) }}" alt=""></li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div style="display: none">
            <div id="imgPopup" class="content__productCard__imgPopup"><i id="imgPopupClose" class="icon-cancel"></i>
                <div class="slideshowPopup">
                    <ul id="slideshowPopup" class="slideshow">
                        @if(count($drug->images))
                            @foreach($drug->images as $image)
                                <li><img src="{{ asset($image->path) }}" alt="" style="max-height: 100%"></li>
                            @endforeach
                        @endif
                    </ul>
                    <ul id="slideshowNavPopup" class="slideshowNav">
                        @if(count($drug->images))
                            @foreach($drug->images as $image)
                                <li><img src="{{ asset($image->path) }}" alt=""></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <div class="packagingOptionsPopup">
                    @if ($firstPortions)
                        @foreach ($firstPortions as $key => $item)

                            <form action="{{ route('cart.add') }}" method="POST">
                                @csrf
                                <input type="hidden" name="code" value="{{ $item->id }}">
                                <div class="packaging"><p class="value"><i class="{{ \App\Models\DrugPortion::getIconByType($item->type) }}"></i> {{ $item->title }}</p>
                                    <p class="price">{{ $item->price }}<span>&nbsp;грн</span></p>
                                    <div id="{{ $item->drug->code }}_{{ $key }}" class="packagingQuantitu">
                                        <span class="minus">-</span>
                                        <input type="text" name="quantity" value="1">
                                        <span class="plus">+</span>
                                    </div>
                                    <button type="submit">Купити</button>
                                </div>
                            </form>
                        @endforeach
                    @endif
                    @if ($lastPortions)
                        @foreach ($lastPortions as $key => $item)
                            <form action="{{ route('cart.add') }}" method="POST">
                                @csrf
                                <input type="hidden" name="code" value="{{ $item->id }}">
                                <div class="packaging"><p class="value"><i class="{{ \App\Models\DrugPortion::getIconByType($item->type) }}"></i> {{ $item->title }}</p>
                                    <p class="price">{{ $item->price }}<span>&nbsp;грн</span></p>
                                    <button type="submit">Купити</button>
                                    <div id="{{ $item->drug->code }}_{{ $key }}" class="packagingQuantitu">
                                        <span class="minus">-</span>
                                        <input type="text" name="quantity" value="1">
                                        <span class="plus">+</span></div>
                                </div>
                            </form>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="content__productCard__packagingOptions">
            @if ($firstPortions)
                @foreach ($firstPortions as $key => $item)

                    <form action="{{ route('cart.add') }}" method="POST">
                        @csrf
                        <input type="hidden" name="code" value="{{ $item->id }}">
                        <div class="packaging"><p class="value"><i class="{{ \App\Models\DrugPortion::getIconByType($item->type) }}"></i> {{ $item->title }}</p>
                            <p class="price active_price" data-price="{{ $item->price }}">{{ $item->price }}<span>&nbsp;грн</span></p>
                            <button type="submit">Купити</button>
                            <div id="{{ $item->drug->code }}_{{ $key }}" class="packagingQuantitu active_quantity">
                                <span class="minus">-</span>
                                <input type="text" name="quantity" value="1">
                                <span class="plus">+</span></div>
                        </div>
                    </form>
                @endforeach
            @endif
            <div class="others"><p>Інші форми випуску&nbsp;&nbsp;&nbsp;<i class="icon-down-arrow"></i></p>
                <div class="othersFormOfSale">
                    @if ($lastPortions)
                        @foreach ($lastPortions as $key => $item)
                            <form action="{{ route('cart.add') }}" method="POST">
                                @csrf
                                <input type="hidden" name="code" value="{{ $item->id }}">
                                <div class="packaging"><p class="value"><i class="{{ \App\Models\DrugPortion::getIconByType($item->type) }}"></i> {{ $item->title }}</p>
                                    <p class="price">{{ $item->price }}<span>&nbsp;грн</span></p>
                                    <button type="submit">Купити</button>
                                    <div id="{{ $item->drug->code }}_{{ $key }}" class="packagingQuantitu">
                                        <span class="minus">-</span>
                                        <input type="text" name="quantity" value="1">
                                        <span class="plus">+</span></div>
                                </div>
                            </form>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="productInfo">

                <dl>
                    <dt>Повна назва:</dt> <dd>{{ $drug->title }}</dd>
                </dl>
                <dl>
                    <dt>Виробник:</dt> <dd>{{ $drug->maker }}</dd>
                </dl>
                <dl>
                    <dt>Міжнародна назва:</dt> <dd>{{ $drug->int_title }}</dd>
                </dl>
                <dl>
                    <dt>Код АТС:</dt> <dd>{{ $drug->ats_code }}</dd>
                </dl>
            </div>
            <div class="consultation"><a href="{{ asset( $drug->pdf_path) }}"><span><i
                                class="imf icon-book"></i></span><span>Переглянути каталог</span></a></div>
            <div class="consultation"><a href="viber://chat?number=+380639562389"><span><i
                                class="icon-viber"></i></span><span>Консультація у Viber</span></a></div>
        </div>
        <div id="descrTabs" class="content__productCard__analogue">
            <ul class="productCard__descr">
                <li><a href="#tabs-1">Фарм опіка</a></li>
                <li><a href="#tabs-2">Інструкція</a></li>
                <li><a href="#tabs-3">Аналоги</a></li>
                <li><a href="#tabs-4">Відеоогляд</a></li>
            </ul>
            <div id="tabs-1" class="farmCare"><p>{{ $drug->farm_ward }}</p></div>
            <div id="tabs-2" class="manual"><p>{{ $drug->instruction }}</p></div>
            <div id="tabs-3" class="therapeuticEffect">
                <div class="completeAnalogue"><h6>Повний аналог</h6><span>(повний збіг складу діючих речовин, їх дозування і форми випуску)</span>
                    <ul>
                        {!! $first = false !!}
                        @foreach($drug->analogs as $analog)
                            @if($analog->type === 1)
                                @php $first = true @endphp
                                <li>
                                    @include('layouts.drug_element', ['drug' => $analog->analog])
                                </li>
                            @endif
                        @endforeach
                        @if(!$first)
                            <p class="ml-em">Не знайдено</p>
                        @endif
                    </ul>
                </div>
                <div class="otherReleseForm"><h6>Аналог в інших формах випуску</h6><span>(збіг складу діючих речовин і їх дозування)</span>
                    <ul>
                        {!! $second = false !!}
                        @foreach($drug->analogs as $analog)
                            @if($analog->type === 2)
                                @php $second = true @endphp
                                <li>
                                    @include('layouts.drug_element', ['drug' => $analog->analog])
                                </li>
                            @endif
                        @endforeach
                        @if(!$second)
                            <p class="ml-em">Не знайдено</p>
                        @endif
                    </ul>
                </div>
                <div class="similarEffect"><h6>Подібний терапевтичний ефект</h6><span>(препарати входять в одну класифікаційну групу АТС)</span>
                    <ul>
                        {!! $third = false !!}
                        @foreach($drug->analogs as $analog)
                            @if($analog->type === 3)
                                @php $third = true @endphp
                                <li>
                                    @include('layouts.drug_element', ['drug' => $analog->analog])
                                </li>
                            @endif
                        @endforeach
                        @if(!$third)
                            <p class="ml-em">Не знайдено</p>
                        @endif
                    </ul>
                </div>
            </div>
            <div id="tabs-4" class="videoSurveillance">
                <p>
                    @if ($drug->youtube_link)
                        <iframe style="width: 100%" width="729" height="410" src="https://www.youtube.com/embed/{{substr($drug->youtube_link, strpos($drug->youtube_link, 'v=')+2)}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    @endif
                </p>
            </div>
        </div>
        <div class="content__recomend"><h5>Радимо придбати</h5>
            <div class="slider">
                <ul id="slideshowRecomend">
                    @foreach($actualProducts as $product)
                        <li>
                            @include('layouts.product_element', ['product' => $product])
                        </li>
                    @endforeach
                </ul>
                <div class="center"><a href="#" class="prevRecomend"><i class="icon-left-arrow"></i></a><a href="#"
                                                                                                           class="nextRecomend"><i
                                class="icon-right-arrow"></i></a></div>
            </div>
        </div>
        <div class="content__howToMakeOrder">
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-shopping-cart cart"></i></p>
                <p class="learnMore"><span id="orderPopup">Дізнатись більше</span></p>
                <p class="info"><span>Як зробити замовлення</span><br><span>Пошук та вибір товару</span></p></div>
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-box box"></i></p>
                <p class="learnMore"><span id="deliveryPopup">Дізнатись більше</span></p>
                <p class="info"><span>Доставка і оплата</span><br><span>Способи, строки, умови</span></p></div>
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-shipped truck"></i></p>
                <p class="learnMore"><span id="underOrderPopup">Дізнатись більше</span></p>
                <p class="info"><span>Товари під замовлення</span><br><span>Безкоштовна доставка</span></p></div>
        </div>
    </div>
</div>

@include('layouts.footer')