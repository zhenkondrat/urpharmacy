@include('layouts.header')
@include('layouts.nav', ['pageHeader' => true])
@if (isset($loginPage) && $loginPage)
    <div id="login-page"></div>
@elseif (isset($registerPage) && $registerPage)
    <div id="register-page"></div>
@endif
<div class="content main">
    <div class="container">
        <div class="content__actualProduct">
            {{-- Actual products --}}
            @if(isset($actualProducts) && $actualProducts)
            <div class="content__actualProduct__headline"><h5>Актуальний товар</h5>
                <div>
                    <p>Лікарські засоби і вироби медичного призначення. Лише
                        сертифікована продукція в Хмельницькому і по всій Україні.</p>
                </div>
            </div>
            <div class="content__actualProduct__slider"><img src="images/actual_products.jpg" alt="img">
                <div class="slider">
                    <ul id="slideshowActual">
                        @foreach($actualProducts as $product)
                        <li>
                            @include('layouts.product_element', ['product' => $product])
                        </li>
                        @endforeach
                    </ul>
                    <div class="center"><a href="#" class="prevActual"><i class="icon-left-arrow"></i></a>
                        <a href="#" class="nextActual"><i class="icon-right-arrow"></i></a>
                    </div>
                </div>
            </div>
            <div class="content__actualProduct__showMore">
                <a href="{{ route('pages.page', 'actual') }}">Показати більше&nbsp;&nbsp;<i class="icon-reply-all"></i></a>
            </div>
            @endif
        </div>
        {{-- Discounted products --}}
        @if(isset($discountProducts) && $discountProducts)
        <div class="content__saleProduct">
            <div class="content__saleProduct__headline"><h5>Акції</h5>
                <div>
                    <p>Лікарські засоби і вироби медичного призначення. Лише
                        сертифікована продукція в Хмельницькому і по всій Україні.</p>
                </div>
            </div>
            <div class="content__saleProduct__slider"><img src="images/sale.jpg" alt="img">
                <div class="slider">
                    <ul id="slideshowSale">
                        @foreach($discountProducts as $product)
                            <li>
                                @include('layouts.product_element', ['product' => $product])
                            </li>
                        @endforeach
                    </ul>
                    <div class="center"><a href="#" class="prevSale"><i class="icon-left-arrow"></i></a>
                        <a href="#" class="nextSale"><i class="icon-right-arrow"></i></a>
                    </div>
                </div>
            </div>
            <div class="content__saleProduct__showMore">
                <a href="{{ route('pages.page', 'discount') }}">Показати більше&nbsp;&nbsp;<i class="icon-reply-all"></i></a>
            </div>
        </div>
        @endif
        <div class="content__howToMakeOrder">
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-shopping-cart cart"></i></p>
                <p class="learnMore"><span id="orderPopup">Дізнатись більше</span></p>
                <p class="info"><span>Як зробити замовлення</span><br><span>Пошук та вибір товару</span></p></div>
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-box box"></i></p>
                <p class="learnMore"><span id="deliveryPopup">Дізнатись більше</span></p>
                <p class="info"><span>Доставка і оплата</span><br><span>Способи, строки, умови</span></p></div>
            <div><p class="icons"><i class="icon-right-arrow-thin arrow"></i><i class="icon-shipped truck"></i></p>
                <p class="learnMore"><span id="underOrderPopup">Дізнатись більше</span></p>
                <p class="info"><span>Товари під замовлення</span><br><span>Безкоштовна доставка</span></p></div>
        </div>
    </div>
</div>
@include('layouts.footer')
<div class="footer">
    <div class="container"><p>Найменування ліцензіата, серія та номер ліцензії, дата видачі,
            юридична адреса, адреса, телефон і контактні дані керівника
            територіального органу центрального органу виконавчої влади, який
            реалізує державну політику у сферах контролю якості та безпеки
            лікарських засобів. пам’ятка для</p>
        <p>Самолікування може бути шкідливим для здоров’я. Лікарські засоби
            належної якості обміну та поверненню не підлягають.</p></div>
</div>