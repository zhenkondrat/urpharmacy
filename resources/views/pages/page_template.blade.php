@include('layouts.header', ['title' => $post->title])
@include('layouts.nav')
<div class="content about__us">
    <div class="container">
        <div class="content__aboutUs__breadcrumb">
            <ul>
                <li><a href="{{ route('pages.index') }}">Головна</a></li>
                <li><span>&nbsp;/&nbsp;</span></li>
                <li><span>{{ $post->title }}</span></li>
            </ul>
        </div>
        <div class="content__aboutUs">
            <div class="content__aboutUs__headline"><h5>{{ $post->title }}</h5></div>
            {!! $post->content !!}
        </div>
    </div>
</div>
@include('layouts.footer')