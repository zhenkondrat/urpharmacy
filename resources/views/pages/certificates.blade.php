@include('layouts.header', ['title' => 'Сертифікати'])
@include('layouts.nav')
<div class="content certificates">
    <div class="container">
        <div class="content__certificate__breadcrumb">
            <ul>
                <li><a href="{{ route('pages.index') }}">Головна</a></li>
                <li><span>&nbsp;/&nbsp;</span></li>
                <li><a href="{{ route('pages.page', 'about_us') }}">Про нас</a></li>
                <li><span>&nbsp;/&nbsp;</span></li>
                <li><span>Сертифікати</span></li>
            </ul>
        </div>
        <div class="content__certificates">
            <div class="content__certificates__headline"><h5>Сертифікати</h5><a href="{{ URL::previous() }}"><i
                    class="icon-left-arrow-thin"></i><span>&nbsp;&nbsp;Повернутися назад</span></a></div>
            <div class="firstBlock"><h6>Підзаголовок</h6>
                <ul>
                    <li class="large"><img id="large" src="" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                </ul>
            </div>
            <div class="secondBlock"><h6>Підзаголовок</h6>
                <ul>
                    <li class="large"><img src="" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                    <li><img src="/images/certificate.jpg" alt=""></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')