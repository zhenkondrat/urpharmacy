<div class="contentAddres">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d2594.9017430322647!2d26.98319256123305!3d49.42967160401825!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sua!4v1523790388116"
            frameborder="0" style="border:0" allowfullscreen></iframe>
    <div class="contentAddres__contactForm">
        <div class="comentary">
            <form id="comentary" action="{{ route('feedback.make') }}" method="post">
                @csrf
                <input type="text" name="nameFeed" class="form-control{{ $errors->has('nameFeed') ? ' err' : '' }}" value="{{ old('nameFeed') }}" required placeholder="Ім’я">
                <input type="tel" name="telFeed" class="js-phone form-control{{ $errors->has('telFeed') ? ' err' : '' }}" value="{{ old('telFeed') }}" required placeholder="Телефон">
                <textarea placeholder="Коментар" class="form-control{{ $errors->has('commentFeed') ? ' err' : '' }}" name="commentFeed" required>{{ old('commentFeed') }}</textarea><br>
                <button type="submit">Надіслати</button>
            </form>
            <div style="display: none">
                <div id="thanksComentary"><i id="thanksComentaryClose" class="icon-cancel"></i>
                    <p>Дякуємо за Ваш коментар!</p>
                    <button type="button">OK</button>
                </div>
            </div>
            @if(session()->has('message'))
            <div style="display: none">
                <div id="customModal"><i id="customModalClose" class="icon-cancel"></i>
                    <p>{{ session()->get('message') }}</p>
                    <button type="button">OK</button>
                </div>
            </div>
            @endif

            @if($errors->first('nameReg') || $errors->first('emailReg') || $errors->first('passwordReg') || $errors->first('phoneReg'))
                <div id="auth-error"></div>
            @endif

            @if($errors->first('email') || $errors->first('passport'))
                <div id="login-error"></div>
            @endif
        </div>
        <div class="contacts">
            <p>Хмельницький, вул. Будівельників, 3</p>
            <p>
                <a href="mailto:urpharmacy.com.ua@gmail.com">email: urpharmacy.com.ua@gmail.com</a>
                <br>
                <a href="tel:+380639562389">тел: (063) 956-23-89</a>
            </p>
        </div>
        <div class="social">
            <ul>
                <li><a href="https://www.facebook.com/" target="_blank"><i class="icon-facebook-logo-button"></i></a>
                </li>
                <li><a href="https://twitter.com/" target="_blank"><i class="icon-twitter-logo-button"></i></a></li>
                <li><a href="https://vk.com" target="_blank"><i class="icon-vk-social-logotype"></i></a></li>
            </ul>
            <p><span>Розробка сайту</span><br><span>CIRCLE WEB GROUP/STUDIO</span></p></div>
    </div>
</div>
<div style="display: none">
    @include('layouts.auth')
    <div id="order"><i id="orderClose" class="icon-cancel"></i><h5>Як зробити замовлення?</h5><br>
        <ul>
            <li>Щоб знайти товар досить ввести в рядку пошуку першіі 3-4 літри назви товару або через «Каталог товарів»</li>
            <li>Виберіть кількість упаковок і натисніть на кнопку «Додати в кошик».</li>
            <li>Після наповнення кошика  необхідними  товарами, натисніть на кнопку «Оформити замовлення».</li>
            <li>Для завершення замовлення необхідно заповнити невелику форму.</li>
            <li>Після заповнення обов'язкових пунктів, натисніть кнопку «Оформити замовлення».</li>
            <li>Найближчим часом Вам передзвонить оператор і уточнить деталі доставки.</li>
        </ul>
    </div>
    <div id="delivery"><i id="deliveryClose" class="icon-cancel"></i><h5>Доставка і оплата</h5><br>
        <p>Будь яка адресна доставка здійснюється на товари, які не являються лікарськими засобами.</p>

        <ul>
        <li>ДОСТАВКА КУР'ЄРОМ У М.ХМЕЛЬНИЦЬКОМУ
            <p>При сумі замовлення від 300 грн. доставка здійснюється безкоштовно.</p>
            <br>
            <p>Графік  роботи служби доставки:</p>
            <p>Будні дні – з 8:00 до 21:00,</p>
            <p>Вихідні дні – 3 9:00 до 18:00</p>

            <p>Наш працівник зв’яжеться з Вами протягом 15 хвилин для пітвердження замовлення.</p>
            <p>Якщо замовлення зроблене у інший час менеджер зв’яжеться з Вами у робочі години.</p>
            <p>Замовлення оформлене(створене на сайті і узгоджене з оператором) до 16:00 буде доставлений в той же день до 22:00</p>
            <p>Замовлення оформлене(створене на сайті і узгоджене з оператором) після 16:00. Буде доставлена з 9:00 до 14:00 наступного дня</p>

            <p>Щоб Ви могли довіряти нашій доставці товарів, ми зобов'язали кур'єра пред'являти клієнтам документи (товарний чек, накладну для юридичних осіб, гарантійний талон для техніки,), а також демонструвати справність техніки (комплектність, цілісність і працездатність пристроїв).</p>

            <p>Копії сертифікатів якості і ліцензій – за попередньою домовленістю.</p>
        </li>
        <li>САМОВИВІЗ

            <p>Якщо Ви бажаєте забрати своє замовлення з аптеки самостійно, для зручності Ви можете попередньо забронювати його по телефону, або через сайт.</p>

            <p>Для пітвердження готовності Вашого замовлення – ми відправимо СМС повідомлення з номером  замовлення. Для отримання замовлення – необхідно працівнику аптеки, вказати номер замовлення  або номер телефону, який був використаний при замовленні.</p>
        </li>
        <li>ДОСТАВКА НОВОЮ ПОШТОЮ

            <p>Доставка здійснюється в межах України при замовленні на суму понад 300 грн з післяплатою.</p>
            <p>Відправлення замовлення день в день, отримання  на складі НП по прибуттю товару(час доставки від 1 до 3х днів).</p>
            <p>Доставка оплачується згідно з тарифами кур’єрської  служби.</p>
        </li>
        </ul>
    </div>
    <div id="underOrder"><i id="underOrderClose" class="icon-cancel"></i><h5>Товари під замовлення</h5><br>
        <p>Доставка рідкісних лікарських засобів або інших дефіцитних товарів,які відсутні на складі, але доступні до поставки від наших партнерів</p>
        <p>Відвантаження товарів під замовлення здійснюється лише за предоплатою в безготівковій формі.</p>
        <br>
        <p>Після того , як Ви здійснили покупку, товар відповідної якості поверненню не підлягає(Постанова КМУ №172 від19.03.94)</p>
    </div>
</div>
<div id="asideButtons" class="asideButtons">
    <ul>
        <li>
            <span class="rollUp">
                <i class="icon-chevron-up"></i>
            </span>
        </li>
        <li>
            <span id="feedbackForm" class="phone">
                <i class="icon-auricular-phone-symbol-in-a-circle"></i>
            </span>
        </li>
    </ul>
</div>
<div style="display: none">
    <div id="feedbackFormPopup" class="feedbackFormPopup">
        <h5>Замовити зворотній дзвінок</h5>
        <form id="userData" class="userData" action="{{ route('feedback.make') }}" method="post">
            @csrf
            <input type="text" name="nameCall" class="form-control{{ $errors->has('nameCall') ? ' err' : '' }}" value="{{ old('nameCall') }}" required placeholder="Ім’я">
            <input type="tel" name="telCall" class="js-phone form-control{{ $errors->has('telCall') ? ' err' : '' }}" value="{{ old('telCall') }}" required placeholder="Телефон">
            <textarea placeholder="Коментар" class="form-control{{ $errors->has('commentCall') ? ' err' : '' }}" name="commentCall" required>{{ old('commentCall') }}</textarea><br>
            <span>Час дзвінка сьогодні</span>
            <p>
                З
                <input type="number" name="timeFrom" max="24" min="0" class="form-control{{ $errors->has('hours') ? ' err' : '' }}" value="{{ old('timeFrom') }}" required>
                До
                <input type="number" name="timeTo" max="24" min="0" class="form-control{{ $errors->has('minutes') ? ' err' : '' }}" value="{{ old('timeTo') }}" required>
            </p>
            <button type="submit">Замовити дзвінок</button>
        </form>
    </div>
</div>
@if(session()->has('cartMessage'))
    <div style="display: none">
        <div id="customModal">
            <p>{{ session()->get('cartMessage') }}</p>
            <a id="modalClose" class="button-custom">Продовжити покупки</a>
            <a href="{{ route('cart.show') }}" class="button-custom">Перейти до корзини</a>
        </div>
    </div>
@endif
@if($errors->first('nameCall') || $errors->first('telCall') || $errors->first('commentCall') || $errors->first('timeFrom') || $errors->first('timeTo'))
    <div id="feedback-call-error"></div>
@endif
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jquery.arcticmodal-0.3.min.js') }}"></script>
<script src="{{ asset('js/jquery.cycle2.js') }}"></script>
<script src="{{ asset('js/jquery.cycle2.carousel.js') }}"></script>
<script src="{{ asset('js/jquery.cycle2.swipe.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script>
        $(".active_quantity input[name='quantity']").change(function () {
            var price = $(this).parent().parent().find(".active_price").attr('data-price');
            var total = price*this.value
            if(!Number.isInteger(total))
                total=total.toFixed(2);
            $(this).parent().parent().find(".active_price").html(total + '<span>&nbsp;грн</span>');
        });

        $("ul .more").on('click', function () {
            $(this).parent().children(".mobil-wrap").toggle();
            $(this).parent().children(".aside-wrap").toggle();

            $(this).toggleClass("show");
        });
</script>