<div id="personalCabinet" class="authorization">
    <div id="authorTabs" class="typeOfAuthor">
        <ul>
            <li><a id="tabs-1-button" href="#tabs-1">Авторизація</a></li>
            <li><a id="tabs-2-button" href="#tabs-2">Реєстрація</a></li>
        </ul>
        <div id="tabs-1" class="author">
            <form id="authorization" action="{{ route('login') }}" method="POST">
                    @csrf
                <p><i class="icon-specialist-user user"></i>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' err' : '' }}"
                           name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail">
                    <i class="icon-asterisk asterisk"></i></p>
                <p><i class="icon-protection-shield shield"></i>
                    <input id="password" type="password"
                           class="form-control{{ $errors->has('password') ? ' err' : '' }}" name="password"
                           required placeholder="Пароль">
                    <i class="icon-asterisk asterisk"></i></p>
                <p><label>
                <p><label><input type="checkbox" name="remember"
                                 class="checkbox" {{ old('remember') ? 'checked' : '' }}><span
                                class="checkbox-custom"></span><span
                                class="label">&nbsp;&nbsp;Запам’ятати мене</span></label></p>
                <button type="submit">Вхід</button>
            </form>
            <a href="#">Забули пароль?</a></div>
        <div id="tabs-2" class="regist">
            <form id="registration" method="POST" action="{{ route('register') }}">
                @csrf
                <p><i class="icon-specialist-user user"></i>
                    @if (!empty($name))
                        <input id="name" type="text" class="form-control{{ $errors->has('nameReg') ? ' err' : '' }}"
                               name="nameReg" value="{{ $name }}" required autofocus placeholder="ПІБ">
                    @else
                        <input id="name" type="text" class="form-control{{ $errors->has('nameReg') ? ' err' : '' }}"
                               name="nameReg" value="{{ old('nameReg') }}" required autofocus placeholder="ПІБ">
                    @endif
                    <i class="icon-asterisk asterisk"></i></p>
                <p><i class="icon-protection-shield shield"></i>
                    <input type="password"
                           class="form-control{{ $errors->has('passwordReg') ? ' err' : '' }}" name="passwordReg"
                           required placeholder="Пароль">
                    <i class="icon-asterisk asterisk"></i></p>
                <p><i class="icon-protection-shield shield"></i>
                    <input id="password-confirm" type="password"
                           class="form-control{{ $errors->has('passwordReg') ? ' err' : '' }}"
                           name="passwordReg_confirmation"
                           required placeholder="Підтвердження паролю">
                    <i class="icon-asterisk asterisk"></i></p>

                <p><i class="icon-telephone-handle-silhouette phone"></i>
                    <input id='phone' type="tel" name="phoneReg" placeholder="тел."
                           class="js-phone form-control{{ $errors->has('phoneReg') ? ' err' : '' }}"
                           value="{{ old('phoneReg') }}" required>
                    <i class="icon-asterisk asterisk"></i>
                </p>
                <p><i class="icon-envelope envelope"></i>
                    @if (!empty($email))
                        <input type="email" class="form-control{{ $errors->has('emailReg') ? ' err' : '' }}" name="emailReg" value="{{ $email }}" required placeholder="E-mail">
                    @else
                        <input type="email" class="form-control{{ $errors->has('emailReg') ? ' err' : '' }}" name="emailReg" value="{{ old('emailReg') }}" required placeholder="E-mail">
                    @endif
                    <i class="icon-asterisk asterisk"></i></p>
                <p><select name="city">
                        <option disabled selected hidden>Виберiть мiсто</option>
                        <option>Хмельницький</option>
                        <option>Рiвне</option>
                    </select></p>
                <button type="submit">Зареєструватися</button>
            </form>
        </div>
    </div>
    <div class="socialAuthor"><p>Увійти за допомогою соцмереж</p>
        <ul>
            <li><a href="{{ Route('login.social', 'facebook') }}" class="facebook"><i class="icon-facebook-logo"></i>&nbsp;&nbsp;Facebook</a>
            </li>
            <li><a href="{{ Route('login.social', 'google') }}" class="gPlus"><i class="icon-google-plus-logo"></i>&nbsp;&nbsp;Google+</a>
            </li>
        </ul>
    </div>
    <div class="close"><p id="authorizationClose">Повернутися до пошуку</p></div>
</div>