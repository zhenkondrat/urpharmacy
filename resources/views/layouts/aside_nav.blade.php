<div id="aside__accordeon" class="content__aside">
    <div class="sale"><a href="{{ route('pages.page', 'discount') }}"><i class="icon-discount"></i>&nbsp;&nbsp;Акції</a></div>
    <div class="aCatalog"><a href="{{ route('pages.page', 'catalog') }}"><i class="icon-list"></i>&nbsp;&nbsp;Каталог товарів</a></div>
    @if (isset($category_types) && $category_types)
        @foreach ($category_types as $type)
            <div class="medicalDevices"><h5><i class="icon-chevron-right dRight"></i><i class="icon-chevron-down dDown"></i>{{$type->name}}</h5>
                <ul>
                    @if (isset($categories) && $categories)
                        @foreach ($categories as $category)
                            @if($category->type === $type->id)
                                @include('layouts.menu_aside_item', ['category' => $category])

                                {{--<li><a style="font-family: 'PTSans-Bold';" href="{{route('category.show', [$category->id])}}">{{ $category->title }}&nbsp;<i class="icon-chevron-right"></i></a>--}}
                                {{--@foreach($category->drugs as $drug)--}}
                                    {{--<li>--}}
                                        {{--<a href="{{ route('product.show', $drug->code) }}">{{ $drug->title }}</a>--}}
                                    {{--</li>--}}
                                    {{--@endforeach--}}
                                {{--</li>--}}
                            @endif
                        @endforeach
                    @endif
                </ul>
            </div>
        @endforeach
    @endif
</div>