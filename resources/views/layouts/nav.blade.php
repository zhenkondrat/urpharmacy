<?php $count = (\Illuminate\Support\Facades\Session::has('quantity')) ? Session::get('quantity') : 0; ?>
<div id="fixedNav" class="header__fixedNav hidden">
    <ul>
        <li id="catalogDrop"><a href="{{ route('pages.page', 'catalog') }}" class="desktop"><i class="icon-list"></i>Каталог товарів</a><a
                    href="#" class="mobile"><i id="mobileDropButton" class="icon-list"></i></a></li>
        <li>
            <div class="desktop">
                <input type="text" placeholder="Пошук препаратів" class="formSearchValue">
                <i class="icon-magnifier submitForm"></i>
            </div>
            <div class="mobile"><i class="icon-magnifier"></i>
                <input type="text" placeholder="Пошук препаратів" class="formSearchValue">
                <i class="icon-left-arrow-thin back submitForm"></i>
            </div>
        </li>
        <li><a href="tel:+380639562389" class="desktop"><i class="icon-auricular-phone-symbol-in-a-circle"></i>&nbsp;&nbsp;0639562389</a><a
                    href="tel:+380639562389" class="mobile"><i class="icon-auricular-phone-symbol-in-a-circle"></i></a>
        </li>
        <li><a href="viber://chat?number=+380639562389"><i class="icon-viber_no_bg"></i></a></li>
        <li><a href="{{ route('cart.show') }}" class="desktop"><i class="icon-shopping-cart"></i>@if($count>0 && !request()->routeIs('cart.show'))<span class="count">{{$count}}</span>@endif&nbsp;&nbsp;Корзина</a><a
                    href="{{ route('cart.show') }}" class="mobile"><i class="icon-shopping-cart"></i>@if($count>0 && !request()->routeIs('cart.show'))<span class="count">{{$count}}</span>@endif</a></li>
    </ul>
</div>
@if (isset($pageHeader) && $pageHeader)

              {{--pump--}}
    <div id="slideshow" class="slider slider-header">

    </div>


    <div class="header__main">
        @endif

        {{--    MY HEADER--}}
        <div class="container-my align-items-center">

            <p class="burger mobileDrop" id="mobileDrop">

            </p>



            <div class="logo-head">
                <a href="{{ route('pages.index') }}"><img src="../images/logo.png" alt=""></a>
            </div>
            <ul class="nav-head d-flex">
                <li><a href="{{ route('pages.page', 'classification') }}" @if(Request::segment(1) === 'classification') class="active_button" @endif>Класифікація АТС</a></li>
                <li><a href="{{ route('pages.page', 'about_us') }}" @if(Request::segment(1) === 'about_us') class="active_button" @endif>Про нас</a></li>
                <li><a href="{{ route('pages.page', 'payment_and_delivery') }}" @if(Request::segment(1) === 'payment_and_delivery') class="active_button" @endif>Оплата і доставка</a></li>
                <li><a href="{{ route('pages.page', 'discount') }}" @if(Request::segment(1) === 'discount') class="active_button" @endif>Акциії</a></li>
                <li><a href="{{ route('pages.page', 'certificates') }}" @if(Request::segment(1) === 'certificates') class="active_button" @endif>Гарантія</a></li>
                <li><a href="{{ route('pages.page', 'contacts') }}" @if(Request::segment(1) === 'contacts') class="active_button" @endif>Контакти</a></li>
            </ul>
            <div class="header-icons">


                <div class="search-new">
                    <form action="" class="search-bar">
                        <input placeholder="         Пошук препаратів" type="search" name="search" pattern=".*\S.*" required>
                        <button class="search-btn" type="submit">
                            <span>Search</span>
                        </button>
                    </form>
                </div>




                <a class="icon-top" id="authorizationPopup{{Auth::user()? 'Authed' : ''}}" href="{{ Auth::user()? route('cabinet.show') : '#' }}">
                    <div class="personal">

                    </div>
                </a>

                <a class="icon-top" id="busket" href="{{ route('cart.show') }}">
                    <div class="basket">

                    </div>
                </a>
            </div>
        </div>

        <div class="mobile-input">
            <div class="d3">
                <form class="input-mobile">
                    <button type="submit"></button>
                    <input type="text" placeholder="Пошук препаратів">
                </form>
            </div>
        </div>



        <div class="header">
            <div class="container">

                    {{--                НИЖНЯЯ ПАНЕЛЬ--}}
                    <div class="header__nav-panel__goods">
                        <ul>
                            @if (isset($category_types) && $category_types)
                                @foreach ($category_types as $type)
                                    <li id="category-{{$type->id}}" class="category" data-url="{{$type->url}}"><span @if(Request::segment(1) === '{{$type->url}}') class="active_button" @endif><i class="{{$type->icon}}"></i>&nbsp;&nbsp;{{$type->name}}</span>
                                        @if (isset($categories) && $categories && collect($categories)->where('type', $type->id)->count() > 0)
                                            <div class="m-wrap">
                                                <ul id="category-{{$type->id}}-drop" class="dropMenu medicalDevices category-drop">
                                                    @foreach ($categories as $category)
                                                        @if($category->type === $type->id)

                                                            @include('layouts.nav_item', ['category' => $category])

                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>




























              {{--ВСПЛЫВАЮЩЕЕ ОКНО СЛЕВА--}}
        <div>
            <div class="header__nav-panel__mobile">

                             {{--ИСХОДНИК--}}

                <div class="header-up-container">
                    <div class="header-mobile">

                        <div id="close" class="close-icon"></div>
                        <div class="title-modal">МЕНЮ</div>
                        <div class="pump"></div>

                    </div>
                </div>

                <div class="tabs">
                    <div class="tab">
                        <input class="input-acordion" type="checkbox" id="chck1">
                        <label class="tab-label" for="chck1">КАТАЛОГ ТОВАРІВ</label>
                        <div class="tab-content">

                            <a href="http://apteka-deploy/catalog" class="item">
                                <div class="item-icon"></div>
                                <div class="item-text">ЛІКАРСЬКІ ЗАСОБИ</div>
                            </a>

                            <a href="http://apteka-deploy/kosmetyka" class="item">
                                <div class="item-icon-2"></div>
                                <div class="item-text">КОСМЕТИКА</div>
                            </a>

                            <a href="http://apteka-deploy/dityachi_tovary" class="item">
                                <div class="item-icon-3"></div>
                                <div class="item-text">ДИТЯЧІ ТОВАРИ</div>
                            </a>

                            <a href="http://apteka-deploy/medtekhnika" class="item">
                                <div class="item-icon-4"></div>
                                <div class="item-text">МЕДТЕХНІКА</div>
                            </a>

                            <a href="http://apteka-deploy/vitaminy_i_bady" class="item">
                                <div class="item-icon-5"></div>
                                <div class="item-text">ВІТАМІНИ І БАДИ</div>
                            </a>

                        </div>
                    </div>
                </div>



                <div class="header__nav-panel__majorMobile">
                    <ul>
                        <li><a href="{{ route('pages.page', 'classification') }}"><i></i>КЛАСИФІКАЦІЯ АТС</a></li>
                        <li><a href="{{ route('pages.page', 'about_us') }}"><i></i>ПРО НАС</a></li>
                        <li><a href="{{ route('pages.index') }}"><i></i>ОПЛАТА І ДОСТАВКА</a></li>
                        <li><a href="{{ route('pages.page', 'discount') }}"><i></i>АКЦІЇ</a></li>
                        <li><a href="{{ route('pages.page', 'certificates') }}"><i></i>ГАРАНТІЯ</a></li>
                        <li><a href="{{ route('pages.page', 'contacts') }}"><i></i>КОНТАКТИ</a></li>
                    </ul>
                </div>
            </div>
        </div>






















{{--              ИСХОДНИК --}}

{{--              <div class="header__nav-panel__goodsMobile">--}}
{{--                  <ul>--}}
{{--                      <li>--}}
{{--                          <a href="{{ route('pages.index') }}"><img src="/images/logo.png" alt=""></a>--}}
{{--                      </li>--}}

{{--                      <li class="more">--}}
{{--                          <a href="http://urpharmacy/catalog"><i class="icon-list"></i>&nbsp;&nbsp;Каталог товарів</a>--}}
{{--                      </li>--}}

{{--                      @if (isset($category_types) && $category_types)--}}
{{--                          @foreach ($category_types as $type)--}}
{{--                              <li>--}}
{{--                                  <a href="{{ route('pages.page', $type->url) }}"><i class="{{$type->icon}}"></i>&nbsp;&nbsp;{{$type->name}}</a>--}}
{{--                                  @if (isset($categories) && $categories && collect($categories)->where('type', $type->id)->count() > 0)--}}
{{--                                      <div class="more"><i class="icon-chevron-right"></i></div>--}}
{{--                                      <div class="mobil-wrap">--}}
{{--                                          <ul id="category-{{$type->id}}-drop" class="dropMenu medicalDevices category-drop">--}}
{{--                                              @foreach ($categories as $category)--}}
{{--                                                  @if($category->type === $type->id)--}}
{{--                                                      @include('layouts.menu_nav_item', ['category' => $category])--}}
{{--                                                  @endif--}}
{{--                                              @endforeach--}}
{{--                                          </ul>--}}
{{--                                      </div>--}}
{{--                                  @endif--}}
{{--                              </li>--}}
{{--                          @endforeach--}}
{{--                      @endif--}}
{{--                  </ul>--}}
{{--              </div>--}}



