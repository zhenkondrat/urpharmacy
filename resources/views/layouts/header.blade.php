<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--change mobile browser color too custom-->
    <meta name="theme-color" content="#00929e"><!--favicon WM-->
    <meta name="msapplication-TileColor" content="#00929e">
    <meta name="msapplication-TileImage" content="{{ asset('images/mstile_144x144.png') }}">
    <meta name="msapplication-config" content="{{ asset('browserconfig.xml') }}"><!--favicon android-->
    <link rel="icon" type="image/png" href="{{ asset('images/favicon_16x16.png') }}"
          sizes="16x16">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon_32x32.png') }}"
          sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon_96x96.png') }}"
          sizes="96x96">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon_192x192.png') }}"
          sizes="192x192"><!--favicon iOs-->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/apple-touch-icon_57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/apple-touch-icon_60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/apple-touch-icon_72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/apple-touch-icon_76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/apple-touch-icon_114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/apple-touch-icon_120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/apple-touch-icon_144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/apple-touch-icon_152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-touch-icon_180x180.png') }}">
    <link rel="manifest" href="{{ asset('manifest.json') }}"><!--my own styles-->
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon_16x16.png') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/reset.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fonts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/icomoon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/icomoon-free.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.arcticmodal-0.3.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/media.css') }}">
    <title>{!! $title ?? 'Твоя надійна аптека' !!}</title>
</head>