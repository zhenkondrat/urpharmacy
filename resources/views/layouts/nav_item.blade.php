<li>
    <a href="{{route('category.show', [$category->id])}}">{{ $category->title }}&nbsp;@if(count($category->subcategories)>0)<i class="icon-chevron-right"></i>@endif</a>

    @if(count($category->subcategories)>0)
        <div class="m-wrap">
        <ul class="subMenu">
            @foreach($category->subcategories as $d)
                @include('layouts.nav_item', ['category' => $d])
            @endforeach
        </ul>
        </div>
    @endif
</li>
