<?php

/**
 * Auth user routes
 */
Auth::routes();
Route::get('/login/{social}', 'Auth\LoginController@socialLogin')->where('social','facebook|google')->name('login.social');
Route::get('/login/{social}/callback', 'Auth\LoginController@handleProviderCallback')->where('social','facebook|google');

/**
 * Main page
 */
Route::get('/', 'PageController@index')->name('pages.index');
Route::get('/mail', function () {
    return view('mail.register');
});

/**
 * Product page
 */
Route::get('product/{code}', 'DrugController@show')->name('product.show');
Route::get('category/{code}', 'DrugController@showCategory')->name('category.show');

/**
 * Product search
 */
Route::get('search/{word}', 'PageController@search')->name('product.search');

/**
 * Cart routes
 */
Route::get('cart', 'CartController@show')->name('cart.show');
Route::post('cart', 'CartController@add')->name('cart.add');
Route::get('cart/clear', 'CartController@clear')->name('cart.clear');

Route::post('cart/change', 'CartController@changeQuantity')->name('cart.change');
Route::post('cart/remove', 'CartController@remove')->name('cart.remove');

/**
 * User routes
 */
Route::post('user/general', 'ProfileController@changeGeneral')->name('user.change.general');
Route::post('user/email', 'ProfileController@changeEmail')->name('user.change.email');
Route::post('user/password', 'ProfileController@changePassword')->name('user.change.password');

/**
 * User Cabinet page
 */
Route::get('cabinet', 'ProfileController@show')->name('cabinet.show');

/**
 * Make order
 */
Route::post('order/make', 'OrderController@make')->name('order.make');

/**
 * Classification
 */
Route::get('classification', 'CategoryController@classificationShow')->name('classification.show');
Route::get('classification/{id}', 'CategoryController@classificationShow')->name('sub-classification.show');

/**
 * Feedback
 */
Route::post('feedback', 'FeedbackController@create')->name('feedback.make');

/**
 * Favorites
 */
Route::post('favorite', 'FavoriteController@add')->name('favorite.add');
Route::post('favorite/remove', 'FavoriteController@remove')->name('favorite.remove');

/**
 * Pages controller route
 */
//Route::get('/ajax/{page}', 'PageController@show')->name('pages.ajax');
Route::get('/{page}', 'PageController@show')->name('pages.page');
