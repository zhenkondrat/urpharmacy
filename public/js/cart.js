$('.changeQty').change(function () {
    const CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        type: "POST",
        url: '/cart/change',
        data: {
            _token: CSRF_TOKEN,
            'id': $(this).data('id'),
            'qty': this.value
        },
        dataType: 'JSON',
        beforeSend: function () {
            // $('.isProcess').attr('data-status', 'true')
        },
        success: function (res) {
            $('.isProcess').attr('data-status', 'false')
            $('#totalCart').text(res.total)
        }
    });
});

$('.deleteItem').click(function () {
    const CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        type: "POST",
        url: '/cart/remove',
        data: {
            _token: CSRF_TOKEN,
            'id': $(this).data('id')
        },
        dataType: 'JSON',
        beforeSend: function () {
            // $('.isProcess').attr('data-status', 'true')
        },
        success: function (res) {
            $('.isProcess').attr('data-status', 'false')
            $('#totalCart').text(res.total)
        }
    });
});


$(".busket .product-wrapper img").each(function( index ) {
    var image = $(this);
    if(image.height() > 120 ){
        image.css({"width": "auto", "height": "100%"});
    }
});