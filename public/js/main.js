"use strict";

function reinit_mobile_drop() {
    if ($(window).width() < 740) return !0;
    $(".header__nav-panel__mobile").arcticmodal("close")
}

$(document).ready(function () {
    $("#slideshow").cycle({fx: "scrollHorz", pauseOnHover: !0, swipe: !0, speed: 500})
}),
$(document).ready(function () {
    $("#authorizationPopup").click(function () {
        $("#personalCabinet").arcticmodal()
    }), $("#authorizationClose").click(function () {
        $("#personalCabinet").arcticmodal("close")
    })
}),
$(document).ready(function () {
    if ($("#auth-error").length || $("#register-page").length) {
        $("#authorizationPopup").click();
        $("#tabs-2-button").click();
    } else if ($("#login-error").length || $("#login-page").length) {
        $("#authorizationPopup").click();
    }
}),
    $("#authorTabs").tabs(), jQuery.validator.addMethod("checkMask", function (e, i) {
    return /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/g.test(e)
}), $(".js-phone").mask("+380(99)999-9999", {autoclear: !1}), $.validator.addMethod("lettersEng", function (e, i) {
    return this.optional(i) || e == e.match(/^[a-zA-Z0-9\s]*$/)
}), $.validator.addMethod("lettersEngRus", function (e, i) {
    return this.optional(i) || e == e.match(/^[а-яА-ЯёЁa-zA-Z\s]+$/)
}), $(document).ready(function () {
}), $(document).ready(function () {

}), $(function (e) {
    var i = e("#fixedNav");
    e(window).scroll(function () {
        i["fade" + (400 < e(this).scrollTop() ? "In" : "Out")](500)
    })
}), $(".mobile input").hide(), $(".mobile .back").hide(), $("#fixedNav ul li:nth-child(2) .mobile i").click(function () {
    $(".mobile input").show(), $(".mobile .back").show()
}), $(".mobile .back").click(function () {
    $(".mobile input").hide(), $(".mobile .back").hide()
}), $(function (t) {
    t(document).mouseup(function (e) {
        var i = t(".mobile input");
        i.is(e.target) || 0 !== i.has(e.target).length || (i.hide(), t(".mobile .back").hide())
    })
}), $(function (e) {
    var i = e("#asideButtons");
    e(window).scroll(function () {
        i["fade" + (800 < e(this).scrollTop() ? "In" : "Out")](500)
    })
}), $(function () {
    $(".rollUp").click(function () {
        return $("html, body").animate({scrollTop: 0}, 1e3), !1
    })
}), $(document).ready(function () {
    $("#feedbackForm").click(function () {
        $("#feedbackFormPopup").arcticmodal()
    })
}),
$(document).ready(function () {
    $(".category-drop").hide(),
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || ($(".category").mouseenter(function () {
        $(this).find(".category-drop").show()
    }),
    $(".category").mouseleave(function () {
        $(".category-drop").hide()
    })),
    $(".category").click(function () {
        var url = $(this).data("url");
        location.href = "/" + url
    })
}),
$(document).ready(function () {
    $("#mobileDrop").click(function () {

        $(".header__nav-panel__mobile").arcticmodal({
            beforeOpen: function () {
                $(".header__nav-panel__mobile").animate({left: 0}, 400)
            }, beforeClose: function () {
                $(".header__nav-panel__mobile").animate({left: "-600px"}, 400)
            }
        })
    })
    $("#mobileDropButton").click(function () {
        $(".header__nav-panel__mobile").arcticmodal({
            beforeOpen: function () {
                $(".header__nav-panel__mobile").animate({left: 0}, 400)
            }, beforeClose: function () {
                $(".header__nav-panel__mobile").animate({left: "-600px"}, 400)
            }
        })
    })
});

$(document).ready(function () {
    $("#close").click(function () {
        $(".header__nav-panel__mobile").arcticmodal("close")
    })
});





var reinitTimerMobileDrop = void 0;

$(window).resize(function () {
    clearTimeout(reinitTimerMobileDrop);
    reinitTimerMobileDrop = setTimeout(reinit_mobile_drop, 50);
});
function reinit_mobile_sort() {
    if ($(window).width() < 1024) return !0;
    $("#catalog__options__mobile").arcticmodal("close")
}


function initCycleActual() {   
    var e = $(document).width();
    let count = 0;
    e < 600 ? count = 1 : 600 < e && e < 980 ? count = 2 : 980 < e && e < 1300 ? count = 3 : 1300 < e && e < 1920 ? count = 4 : count = 5

    $("#slideshowActual").cycle({
        fx: "carousel",
        timeout: 0,
        slides: "> li",
        swipe: !0,
        swipeFx: "carousel",
        carouselVisible: count,
        prev: ".prevActual",
        next: ".nextActual"
    })

    // $("#slideshowActual").width($("#slideshowActual").width() * count);
}

function reinit_cycle_actual() {
    var e = $(window).width();
    let count = 0;
    e < 600 ? count = 1
        : 600 < e && e < 980 ? count = 2
        : 980 < e && e < 1300 ? count = 3
        : 1300 < e && e < 1800 ? count = 4
        : count = 5;

   $("#slideshowActual").cycle("destroy");
   reinitCycleActual(count);

}

function reinitCycleActual(e) {
    $("#slideshowActual").cycle({
        fx: "carousel",
        timeout: 0,
        slides: "> li",
        next: ".nextActual",
        prev: ".prevActual",
        swipe: !0,
        swipeFx: "carousel",
        carouselVisible: e
    })


    $("#slideshowActual").width($("#slideshowActual").width() * count);
}


$(".content__actualProduct__headline div p span").click(function (e) {
    e.preventDefault();
    $(".content__actualProduct__headline div p span").removeClass("active");
    $(this).addClass("active");
});

$(".content__saleProduct__headline div p span").click(function (e) {
    e.preventDefault();
    $(".content__saleProduct__headline div p span").removeClass("active");
    $(this).addClass("active");
});

initCycleActual();

var reinitTimerActual = void 0;

$(window).resize(function () {
    clearTimeout(reinitTimerActual), reinitTimerActual = setTimeout(reinit_cycle_actual, 100)
});

function initCycleSale() {
    var e = $(document).width();
    var count = 0;
    e < 600 ? count = 1
        : 600 < e && e < 980 ? count = 2
        : 980 < e && e < 1300 ? count = 3
        : 1300 < e && e < 1800 ? count = 4
        : count = 5;

    $("#slideshowSale").cycle({
        fx: "carousel",
        timeout: 0,
        slides: "> li",
        carouselVisible: count,
        swipe: !0,
        swipeFx: "carousel",
        prev: ".prevSale",
        next: ".nextSale"
    });

    // $("#slideshowSale").width($("#slideshowActual").width());
}

function reinit_cycle_sale() {
    var e = $(window).width();
    var count = 0;
    e < 600 ? count = 1
        : 600 < e && e < 980 ? count = 2
        : 980 < e && e < 1300 ? count = 3
            : 1300 < e && e < 1800 ? count = 4
                : count = 5;
    $("#slideshowSale").cycle("destroy");
    reinitCycleSale(count);
}

function reinitCycleSale(e) {
    $("#slideshowSale").cycle({
        fx: "carousel",
        timeout: 0,
        slides: "> li",
        next: ".nextSale",
        prev: ".prevSale",
        swipe: !0,
        swipeFx: "carousel",
        carouselVisible: e
    })
}

initCycleSale();

var reinitTimerSale = void 0;




$(window).resize(function () {
    clearTimeout(reinitTimerSale), reinitTimerSale = setTimeout(reinit_cycle_sale, 100)
});
$(document).ready(function () {
    $("#orderPopup").click(function () {
        $("#order").arcticmodal()
    }), $("#orderClose").click(function () {
        $("#order").arcticmodal("close")
    })
});
$(document).ready(function () {
    $("#deliveryPopup").click(function () {
        $("#delivery").arcticmodal()
    }), $("#deliveryClose").click(function () {
        $("#delivery").arcticmodal("close")
    })
});
$(document).ready(function () {
    $("#underOrderPopup").click(function () {
        $("#underOrder").arcticmodal()
    }), $("#underOrderClose").click(function () {
        $("#underOrder").arcticmodal("close")
    })
});
$(document).ready(function () {
    $("#mobileSort").click(function () {
        if (!($(document).width() < 1024)) return !1;
        $("#catalog__options__mobile").arcticmodal(), $("#mobileSortClose, #back").click(function () {
            $("#catalog__options__mobile").arcticmodal("close")
        })
    })
});

var reinitTimerMobileSort = void 0;

function reinit_img_popup() {
    var e = $(window).width();
    e < 430 ? ($("#imgPopup").css("destroy"), reinitImgPopup(290)) : 430 < e && e < 780 ? ($("#imgPopup").css("destroy"), reinitImgPopup(400)) : 780 < e && e < 1100 ? ($("#imgPopup").css("destroy"), reinitImgPopup(750)) : ($("#imgPopup").css("destroy"), reinitImgPopup(1050))
}

function reinitImgPopup(e) {
    $("#imgPopup").css("width", e)
}

$(window).resize(function () {
    clearTimeout(reinitTimerMobileSort), reinitTimerMobileSort = setTimeout(reinit_mobile_sort, 50)
});
$(".content__productCard__img > #slideshow").cycle({
    fx: "scrollHorz",
    slides: "> li",
    speed: "fast",
    timeout: 0,
    pager: ".content__productCard__img > #slideshowNav",
    pagerTemplate: ""
});
$(document).ready(function () {
    $("#slideshow li img").click(function () {
        var e = $(document).width();
        e < 430 ? $("#imgPopup").attr("src", $(this).attr("src")).arcticmodal().css("width", "290px") : 430 < e && e < 780 ? $("#imgPopup").attr("src", $(this).attr("src")).arcticmodal().css("width", "400px") : 780 < e && e < 1100 ? $("#imgPopup").attr("src", $(this).attr("src")).arcticmodal().css("width", "750px") : $("#imgPopup").attr("src", $(this).attr("src")).arcticmodal().css("width", "1050px"), $("#imgPopupClose").click(function () {
            $("#imgPopup").arcticmodal("close")
        })
    })
});

var reinitTimerImgPopup = void 0;

function initCycleRecomend() {
    var e = $(document).width();

    var count = 0;
    e < 560 ? count = 1
        : 560 < e && e < 780 ? count = 2
        : 780 < e && e < 1060 ? count = 3
        : 1060 < e && e < 1310 ? count = 4
                : count = 5;

    $("#slideshowRecomend").cycle({
        fx: "carousel",
        timeout: 0,
        slides: "> li",
        swipe: !0,
        swipeFx: "carousel",
        carouselVisible: count,
        prev: ".prevRecomend",
        next: ".nextRecomend"
    });

    $("#slideshowRecomend").width($("#slideshowRecomend li:first-child ").width()*(count) + parseInt(($("#slideshowRecomend li:first-child ").width()+2)/2));}


function reinit_cycle_recomend() {
    var e = $(window).width();
    var count = 0;
    e < 560 ? count = 1
        : 560 < e && e < 780 ? count = 2
        : 780 < e && e < 1060 ? count = 3
        : 1060 < e && e < 1310 ? count = 4
        : count = 5;
   $("#slideshowRecomend").cycle("destroy");
   reinitCycleRecomend(count);
}

function reinitCycleRecomend(e) {
    $("#slideshowRecomend").cycle({
        fx: "carousel",
        timeout: 0,
        slides: "> li",
        next: ".nextRecomend",
        prev: ".prevRecomend",
        swipe: !0,
        swipeFx: "carousel",
        carouselVisible: e
    })
}

initCycleRecomend();
var reinitTimerRecomend = void 0;
$(window).resize(function () {
    clearTimeout(reinitTimerRecomend);
    reinitTimerRecomend = setTimeout(reinit_cycle_recomend, 100);
});

$(window).resize(function () {
    clearTimeout(reinitTimerImgPopup);
    reinitTimerImgPopup = setTimeout(reinit_img_popup, 50);
});
$(".content__productCard__imgPopup > .slideshowPopup > #slideshowPopup").cycle({
    fx: "scrollHorz",
    slides: "> li",
    speed: "fast",
    timeout: 0,
    pager: ".content__productCard__imgPopup >  .slideshowPopup > #slideshowNavPopup",
    pagerTemplate: ""
}), $(".othersFormOfSale").hide(), $(".others p").click(function () {
    $(".othersFormOfSale").is(":hidden") ? ($(this).css("background-color", "#00929E").css("color", "#fff").css("border-bottom-left-radius", "0px").css("border-bottom-right-radius", "0px"), $(".othersFormOfSale").slideToggle("slow")) : $(".othersFormOfSale").is(":visible") && ($(this).css("background-color", "#fff").css("color", "#00929E").css("border-bottom-left-radius", "15px").css("border-bottom-right-radius", "15px"), $(".othersFormOfSale").slideToggle("slow"))
}), $(document).on("click", function (e) {
    $(e.target).closest(".others p").length || $(".othersFormOfSale").hide(), e.stopPropagation()
}),  $("#aside__accordeon ul").hide().prev().click(function () {
    $(this).parents("#aside__accordeon").find("ul").not(this).slideUp().prev().removeClass("active"), $(this).next().not(":visible").slideDown().prev().addClass("active")
}), $("#medDev__accordeon ul").hide().prev().click(function () {
    $(this).parents("#medDev__accordeon").find("ul").not(this).slideUp().prev().removeClass("active"), $(this).next().not(":visible").slideDown().prev().addClass("active")
});
var minus = $(".minus"), plus = $(".plus");

function reinit_firstBlock_certificates() {
    var e = $(window).width();
    e < 736 ? ($("#large").css("destroy"), reinitSecondBlockImg(290)) : 736 < e && e < 1024 ? ($("#large").css("destroy"), reinitSecondBlockImg(450)) : ($("#large").css("destroy"), reinitSecondBlockImg(600))
}

function reinitFirstBlockImg(e) {
    $("#large").css("width", e)
}

plus.click(function () {
    var e = $(this).parent().find("input");
    e.val(parseInt(e.val()) + 1), e.change()
}), minus.click(function () {
    var e = $(this).parent().find("input"), i = parseInt(e.val()) - 1;
    i = i < 1 ? 1 : i, e.val(i), e.change()
}), $("#descrTabs").tabs(), $("body").on("click", ".delete", function (e) {
    $(this).closest("div").remove()
}), $(document).ready(function () {
    $("#costumerInfo").validate({
        rules: {
            name: {required: !0, lettersEngRus: !0, minlength: 3, maxlength: 50},
            tel: {required: !0, checkMask: !0}
        },
        messages: {
            name: {required: !1, lettersEngRus: !1, minlength: !1, maxlength: !1},
            tel: {required: !1, checkMask: !1}
        },
        submitHandler: function () {
            $(document).ready(function () {
                $("#thanksOrder").arcticmodal(), $("#thanksOrderClose, #thanksOrder button").click(function () {
                    $("#thanksOrder").arcticmodal("close")
                })
            })
        }
    })
}), $("#cabinetNav").tabs(), $("#contactsForm, #emailForm, #passwordForm").hide(), $("#contactsEdit").click(function () {
    $("#tel, #lastName, #firstName").hide(), $("#contactsForm").show(), $("#contactsEdit").hide()
}), $("#emailEdit").click(function () {
    $("#email").hide(), $("#emailForm").show(), $("#emailEdit").hide()
}), $("#passwordEdit").click(function () {
    $("#password").hide(), $("#passwordForm").show(), $("#passwordEdit").hide()
}), $(document).ready(function () {
    $("#contactsForm").validate({
        rules: {
            tel: {required: !0, checkMask: !0},
            firstName: {required: !0, lettersEngRus: !0, minlength: 3, maxlength: 50}
        },
        messages: {
            tel: {required: !1, checkMask: !1},
            firstName: {required: !1, lettersEngRus: !1, minlength: !1, maxlength: !1}
        }
    })
}), $(document).ready(function () {
    $("#emailForm").validate({
        rules: {email: {required: !0, email: !0}},
        messages: {email: {required: !1, email: !1}}
    })
}), $(document).ready(function () {
    $("#passwordForm").validate({
        rules: {newPassword: {required: !0}, confirmPassword: {equalTo: "#pswd"}},
        messages: {newPassword: {required: !1}, confirmPassword: {required: !1, equalTo: !1}}
    })
}), $(document).ready(function () {
    $(".myOrders p").click(function () {
        $("#orderInfo").arcticmodal()
    }), $("#infoClose").click(function () {
        $("#orderInfo").arcticmodal("close")
    })
}), $(document).ready(function () {
    $(".firstBlock ul li img").click(function () {
        var e = $(document).width();
        e < 736 ? $("#large").attr("src", $(this).attr("src")).arcticmodal().css("width", "290px") : 736 < e && e < 1024 ? $("#large").attr("src", $(this).attr("src")).arcticmodal().css("width", "450px") : $("#large").attr("src", $(this).attr("src")).arcticmodal().css("width", "600px")
    })
}), $(document).ready(function () {
    if ($("#customModal").length) {
        $("#customModal").arcticmodal(), $("#customModalClose, #customModal button, #modalClose").click(function () {
            $("#customModal").arcticmodal("close")
        })
    }
}), $(document).ready(function () {
    if ($("#feedback-call-error").length) {
        $("#feedbackFormPopup").arcticmodal()
    }
}), $(document).ready(function () {
    $(".formSearchValue").change(function () {
        var value = this.value;
        var form = $('#searchForm');
        var action = form.data('url');

        // set value from input to form action url
        form.attr('action', action + '/' + value);
    });

    $(".submitForm").click(function () {
        $('#searchForm').submit();
    })

    $(".formSearchValue").keyup(function (e) {
        if (e.keyCode == 13) {
            $('#searchForm').submit();
        }
    });
});
var reinitTimerFirstBlock = void 0;

function reinit_secondBlock_certificates() {
    var e = $(window).width();
    e < 736 ? ($("#large").css("destroy"), reinitSecondBlockImg(290)) : 736 < e && e < 1024 ? ($("#large").css("destroy"), reinitSecondBlockImg(450)) : ($("#large").css("destroy"), reinitSecondBlockImg(600))
}

function reinitSecondBlockImg(e) {
    $("#large").css("width", e)
}

$(window).resize(function () {
    clearTimeout(reinitTimerFirstBlock), reinitTimerFirstBlock = setTimeout(reinit_firstBlock_certificates, 50)
}), $(document).ready(function () {
    $(".secondBlock ul li img").click(function () {
        var e = $(document).width();
        e < 736 ? $("#large").attr("src", $(this).attr("src")).arcticmodal().css("width", "290px") : 736 < e && e < 1024 ? $("#large").attr("src", $(this).attr("src")).arcticmodal().css("width", "450px") : $("#large").attr("src", $(this).attr("src")).arcticmodal().css("width", "600px")
    })
});
var reinitTimerSecondBlock = void 0;
$(window).resize(function () {
    clearTimeout(reinitTimerSecondBlock), reinitTimerSecondBlock = setTimeout(reinit_secondBlock_certificates, 50)
});

$(document).ready(function () {
    // initCycleActual();
    // reinit_cycle_actual();
    // initCycleSale();
    // reinit_cycle_sale();
    // initCycleRecomend();
    // reinit_cycle_recomend();
    // $("#slideshowActual").width($("#slideshowSale").width() * 4);
    // console.log($("#slideshowSale").width());
});