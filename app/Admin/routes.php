<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', function () {
        return redirect(route('users.index'));
    });
    $router->resource('/users', 'UserController');
    $router->resource('/drugs', 'DrugController');
    $router->get('/goods/refresh', 'DrugController@refresh');
    $router->resource('/categories', 'CategoryController');
    $router->resource('/category_types', 'CategoryTypeController');
    $router->resource('/ats_categories', 'AtsCategoryController');
    $router->resource('/orders', 'OrderController');
    $router->resource('/hot_products', 'HotProductController');
    $router->resource('/feedbacks', 'FeedbackController');
    $router->resource('/posts', 'PostController');
    $router->resource('/units', 'UnitController');
});
