<?php

namespace App\Admin\Controllers;

use App\Models\CategoryType;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\Category;

class CategoryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Категорії');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування категорій');
            $content->description('Редагування категорій');

            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування категорій');
            $content->description('Редагування категорій');

            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Створення категорії');
            $content->description('Створення нової категорії');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Category::class, function (Grid $grid) {

            $grid->title('Назва')->sortable();
            $grid->slug('Slug')->sortable();
            $grid->abbreviation('Абревіатура')->sortable();
            $grid->category_id('НадКатегорія')->display(function () {
                return $this->headcategory['title'] ?? '';
            })->sortable();
            $grid->type('Розділ')->display(function () {
                return CategoryType::getTypes()[$this->type] ?? '';
            })->sortable();

            $grid->disableExport();

        });
    }

    /**
     * Make a form builder.
     *
     * @param null $id
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(Category::class, function (Form $form) use ($id) {
            $form->text('title', 'Назва')->rules('required|string|max:191');
            $form->text('slug', 'Slug')->rules(function () use ($id) {
                if ($id) {
                    return 'unique:categories,slug,' . $id;
                }

                return 'unique:categories,slug';
            })->readOnly();
            $form->text('abbreviation', 'Абревіатура')->rules('nullable|string|max:32');
            $form->select('category_id', 'НадКатегорія')
                ->options(Category::all()->pluck('title', 'id'));
            $form->select('type', 'Розділ')
                ->options(CategoryType::getTypes());

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableList();
            });

            $form->disableEditingCheck();
            $form->disableViewCheck();
            $form->disableReset();
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        $this->prepareInput();

        return $this->form()->store();
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id)
    {
        $this->prepareInput();

        return $this->form($id)->update($id);
    }

    private function prepareInput()
    {
        $data['slug'] = slugify(request()->title);

        request()->merge($data);
    }
}
