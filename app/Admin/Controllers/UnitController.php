<?php

namespace App\Admin\Controllers;

use App\Models\Unit;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class UnitController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Тип упаковки');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування типу упаковки ');
            $content->description('Редагування типу упаковки');

            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування типу упаковки');
            $content->description('Редагування типу упаковки');

            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Створення типу упаковки');
            $content->description('Створення нового типа упаковки');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Unit::class, function (Grid $grid) {
            $grid->name('Назва')->sortable();
            $grid->order('Порядок');
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @param null $id
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(Unit::class, function (Form $form) use ($id) {
            $form->text('name', 'Назва')->rules('required|string|max:191');
            $form->number('order', 'Порядок')->default(0    );

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableList();
            });

            $form->disableEditingCheck();
            $form->disableViewCheck();
            $form->disableReset();
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        return $this->form()->store();
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id)
    {
        return $this->form($id)->update($id);
    }

}
