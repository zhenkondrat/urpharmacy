<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Користувачі');

            $content->body($this->grid());
        });
    }

    /**
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування користувача');
            $content->description('Редагування користувача');

            $content->body($this->form(true)->edit($id));
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редагування користувача');
            $content->description('Редагування користувача');

            $content->body($this->form(true)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Створення користувача');
            $content->description('Створення нового користувача');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(User::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->name('ПІБ');
            $grid->email('Email');
            $grid->phone('Телефон');

            $grid->created_at('Дата реєстрації');

            $grid->filter(function($filter) {
                $filter->like('name', 'ПІБ');
                $filter->like('email', 'Email');
                $filter->like('phone', 'Телефон');
            });

            $grid->disableExport();

        });
    }

    /**
     * Make a form builder.
     *
     * @param bool $isEdit
     * @return Form
     */
    protected function form($isEdit = false)
    {
        return Admin::form(User::class, function (Form $form) use ($isEdit) {

            $form->display('id', 'ID');
            $form->text('name', 'ПІБ')->rules('required|string|max:191');
            $form->text('email', 'Email')->rules(function ($form) {
                return 'required|string|email|max:255|unique:users,email,' . $form->model()->id . ',id';
            });
            $form->text('phone', 'Телефон')->rules('required|string|max:191');

            if(!$isEdit) {
                $form->hidden('password', 'Пароль')->value(Hash::make(str_random(8)));
            }

            $form->display('created_at', 'Створено о');
            $form->display('updated_at', 'Оновлено о');

            $form->tools(function (Form\Tools $tools) {
                $tools->disableView();
                $tools->disableList();
            });

            $form->disableEditingCheck();
            $form->disableViewCheck();
            $form->disableReset();
        });
    }
}
