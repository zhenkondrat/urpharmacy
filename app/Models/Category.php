<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property int|null $category_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Drug[] $drugs
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereTitle($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Category $headcategory
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $subcategories
 */
class Category extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'abbreviation',
        'description',
        'category_id',
        'type'
    ];

//    public const TYPES = [
//        1 => 'Лікарські засоби',
//        2 => 'Косметика',
//        3 => 'Дитячі товари',
//        4 => 'Медтехніка',
//        5 => 'Вітаміни і БАДи'
//    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function drugs()
    {
        return $this->hasMany(Drug::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subcategories()
    {
        return $this->hasMany(Category::class, 'category_id', 'id')->with('subcategories');
    }

    public static function getall(Category $category) {
        $all_ids = [];
        if ($category->subcategories->count() > 0) {
            foreach ($category->subcategories as $child) {
                $all_ids[] = $child->id;
                $all_ids=array_merge($all_ids,is_array(Category::getall($child))?Category::getall($child):[] );
            }
        }
        return $all_ids;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function headcategory()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category_type()
    {
        return $this->belongsTo(CategoryType::class, 'type', 'id');
    }
}
