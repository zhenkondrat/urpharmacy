<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryType extends Model
{
    protected $table = 'category_types';
    protected $primaryKey='id';

    protected $fillable = [
        'name', 'order',
    ];

    public static function getTypes(){
        return CategoryType::get()->pluck('name', 'id')->toArray();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany(Category::class, 'type', 'id');
    }
}
