<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'units';
    protected $primaryKey='id';

    protected $fillable = [
        'name', 'order',
    ];

    public static function getUnits(){
        return Unit::get()->pluck('name', 'id')->toArray();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany(DrugPortion::class, 'type', 'id');
    }
}
