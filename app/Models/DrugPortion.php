<?php

namespace App\Models;

use App\Unit;
use Illuminate\Database\Eloquent\Model;
use Auth;

/**
 * App\Models\DrugPortion
 *
 * @property int $id
 * @property int $drug_id
 * @property string $title
 * @property string|null $description
 * @property float $price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Drug $drug
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DrugPortion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DrugPortion whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DrugPortion whereDrugId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DrugPortion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DrugPortion wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DrugPortion whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DrugPortion whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DrugPortion extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'drug_id',
        'code',
        'title',
        'description',
        'price',
        'price_get',
        'discount_price',
        'quantity',
        'type'
    ];

//    public const TYPES = [
//        1 => 'Упаковка',
//        2 => 'Пластина',
//        3 => 'Капсули',
//        4 => 'Коробка'
//    ];

    /**
     * @param $type
     * @return null|string
     */
    public static function getIconByType($type)
    {
        switch ($type) {
            case 1:
            case 4:
                return 'icon-package';
            case 2:
            case 3:
                return 'icon-pills';
        }

        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function drug()
    {
        return $this->belongsTo(Drug::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    public function isFavorite()
    {
        $user = Auth::user();

        if ($user) {
            return $this->favorites()
                ->where('user_id', $user->id)
                ->first() ? true : false;
        }

        return false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function units()
    {
        return $this->belongsTo(Unit::class, 'type', 'id');
    }
}
