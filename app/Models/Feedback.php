<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedbacks';

    protected $fillable = [
        'name', 'phone', 'comment', 'status'
    ];

    public const STATUSES = [
        0 => 'Новий',
        1 => 'Відповідь дана',
        2 => 'Завершений'
    ];
}
