<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\Drug
 *
 * @property int $id
 * @property string $title
 * @property string|null $int_title
 * @property string|null $maker
 * @property string|null $ats_coke
 * @property string|null $instruction
 * @property string|null $farm_ward
 * @property array $analogs
 * @property int|null $category_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DrugPortion[] $portions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereAnalogs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereAtsCoke($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereFarmWard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereInstruction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereIntTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereMaker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drug whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Drug extends Model
{
    use Filterable;

    protected $casts = [
        'analogs' => 'array'
    ];

    protected $fillable = [
        'title',
        'code',
        'int_title',
        'maker',
        'ats_code',
        'instruction',
        'farm_ward',
        'analogs',
        'category_id',
        'youtube_link',
        'pdf_path'
    ];

    public static function boot() {
        parent::boot();

        static::deleting(function($drug) {
            $drug->analogs()->delete();
            $drug->hotProducts()->delete();
            $drug->portions()->delete();

            foreach ($drug->images as $image) {
                Storage::delete('public' . substr($image->path, 7));
            }

            $drug->images()->delete();
        });
    }

    /**
 * @return \Illuminate\Database\Eloquent\Relations\HasMany
 */
    public function portions()
    {
        return $this->hasMany(DrugPortion::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(DrugImages::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hotProducts()
    {
        return $this->hasMany(HotProducts::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function analogs()
    {
        return $this->hasMany(Analog::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function atsCategory()
    {
        return $this->belongsTo(AtsCategory::class);
    }
}
