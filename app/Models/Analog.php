<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Analog extends Model
{
    protected $fillable = [
        'drug_id', 'analog_id', 'type'
    ];

    public const TYPES = [
        1 => 'Повний аналог',
        2 => 'Аналог в інших формах випуску',
        3 => 'Подібний терапевничний ефект'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function drug()
    {
        return $this->belongsTo(Drug::class, 'drug_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function analog()
    {
        return $this->belongsTo(Drug::class, 'analog_id', 'id');
    }


}
