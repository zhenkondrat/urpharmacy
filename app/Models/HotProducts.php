<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotProducts extends Model
{
    protected $fillable = [
        'drug_id',
        'type'
    ];

    public $timestamps = false;

    /**
     * Types of hot products
     */
    public const TYPES = [
        1 => 'Актуальні товари',
        2 => 'Популярні товари'
    ];

    /**
     * Hot products
     */
    public const ACTUAL_PRODUCTS = 1;
    public const POPULAR_PRODUCTS = 2;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function drug()
    {
        return $this->belongsTo(Drug::class);
    }
}
