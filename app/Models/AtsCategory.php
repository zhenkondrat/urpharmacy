<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AtsCategory extends Model
{
    protected $fillable = [
        'title',
        'category_id',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function drugs()
    {
        return $this->hasMany(Drug::class, 'ats_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subcategories()
    {
        return $this->hasMany(AtsCategory::class, 'category_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function headcategory()
    {
        return $this->belongsTo(AtsCategory::class, 'category_id', 'id');
    }
}
