<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Drug;
use App\Models\DrugPortion;
use App\Models\HotProducts;
use App\Repositories\DrugRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class DrugController extends Controller
{
    /**
     * @var DrugRepository
     */
    protected $drug;

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * DrugController constructor.
     *
     * @param DrugRepository $drug
     * @param UserRepository $user
     */
    public function __construct(DrugRepository $drug, UserRepository $user)
    {
        $this->drug = $drug;
        $this->user = $user;
    }

    /**
     * @param $code
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($code)
    {
        $drug = $this->drug->find($code);
        $portions = $this->drug->getAllPortions($drug);

        /**
         * Paginate portions for 1-2 and other
         */
        $firstPortions = null;
        $lastPortions = null;

        if (isset($portions[0])) {
            $firstPortions[] = $portions[0];
        }

        if (isset($portions[1])) {
            $firstPortions[] = $portions[1];
        }

        if (isset($portions[2])) {
            unset($portions[0], $portions[1]);

            $lastPortions = $portions;
        }

        $actualProducts = $this->drug->getActualProductsQuery()->take(8)->get();

        return view('pages.product_card', compact('drug', 'firstPortions', 'lastPortions', 'actualProducts'));
    }

    /**
     * @param $code
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCategory($code, Request $request)
    {
        $formType = null;
        $makerType = null;
        $rating = null;

        $category = Category::where('id',$code)->first();

        $categories = Category::getall($category);
        array_push($categories, $category->id);

        $products = DrugPortion::whereHas('drug', function ($query) use ($categories) {
            $query->whereIn('category_id', $categories);
        });

        $category = $category->title;

        if (isset($products) && request()->get('formType') && request()->get('makerType') && request()->get('rating')) {
            $formType = request()->get('formType');
            $makerType = request()->get('makerType');
            $rating = request()->get('rating');

            /**
             * FormTypes filter
             */
            if ($formType !== 'default') {
                $products = $products->whereType($formType);
            }

            /**
             * Maker filter
             */
            if ($makerType !== 'default') {
                $products = $products->whereHas('drug', function ($query) use ($makerType) {
                    $query->whereMaker($makerType);
                });
            }

            /**
             * Sort filter
             */
            if ($rating !== 'default') {
                if ($rating === 'price-cheap') {
                    $products = $products->orderBy('price', 'asc');
                } elseif ($rating === 'price-costly') {
                    $products = $products->orderBy('price', 'desc');
                } elseif ($rating === 'availability') {
                    $products = $products->orderBy('quantity', 'desc');
                }

            }
        }

        if (isset($products)) {
            /**
             * Get Paginated products
             */
            $products = $products->paginate(9);

            if(isset($request->load)){
                $html = view('layouts.list_product_elements')->with(compact('products'))->render();
                $paginator = view('layouts.paginator')->with(compact('products'))->render();


                return response()->json(['success' => true, 'html' => $html, 'paginator' => $paginator]);
            }

            return view('pages.catalog', [
                'products' => $products->appends(Input::except('page')),
                'category' => $category,
                'formType' => $formType,
                'makerType' => $makerType,
                'rating' => $rating
            ]);
        }

        return view('exceptions.not-found');

    }
}
