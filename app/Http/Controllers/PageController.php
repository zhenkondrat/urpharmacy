<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CategoryType;
use App\Models\Drug;
use App\Models\DrugPortion;
use App\Models\Post;
use App\Repositories\DrugRepository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;

class PageController extends Controller
{
    /**
     * @var DrugRepository $drugs
     */
    protected $drugs;

    /**
     * Word for search
     *
     * @var $findWord
     */
    protected $findWord = null;

    /**
     * PageController constructor.
     * @param DrugRepository $drugs
     */
    public function __construct(DrugRepository $drugs)
    {
        $this->drugs = $drugs;
    }

    public function index()
    {
        $actualProducts = $this->drugs->getActualProductsQuery()->take(8)->get();
        $discountProducts = $this->drugs->getDiscountProductsQuery()->take(8)->get();

        return view('pages.index', compact('actualProducts', 'discountProducts'));
    }

    public function search($word, Request $request)
    {
        $this->findWord = $word;

        return $this->show('search', $request);
    }

    /**
     * @param $page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($page, Request $request)
    {
        $category = null;
        $formType = null;
        $makerType = null;
        $rating = null;

        switch ($page) {
            /**
             * Categories
             */
            case 'likarski_zasoby':
            case 'kosmetyka':
            case 'dityachi_tovary':
            case 'medtekhnika':
            case 'vitaminy_i_bady':
                $type = CategoryType::where('url',$page)->firstOrFail();
                $category = $type->name;
                $products = $this->drugs->getByCategoryTypeQuery($type->id);
                break;
            case 'actual':
                $products = $this->drugs->getActualProductsQuery();
                $category = 'Актуальні товари';

                break;
            case 'popular':
                $products = $this->drugs->getPopularProductsQuery();
                $category = 'Популярні товари';

                break;
            case 'discount':
                $products = $this->drugs->getDiscountProductsQuery();
                $category = 'Акційні товари';

                break;
            case 'catalog':
                $products = $this->drugs->getQuery();

                break;
            /**
             * Search
             */
            case 'search':
                if ($this->findWord) {
                    $category = 'Пошук по - ' . $this->findWord;

                    $products = $this->drugs->getSearchQuery($this->findWord);
                } else {
                    $products = $this->drugs->getQuery();
                }

                break;

            /**
             * Other pages
             */
//            case 'about_us':
//                return view('pages.about_us');
//
//            case 'contacts':
//                return view('pages.contacts');
//
            case 'certificates':
                return view('pages.certificates');
            default:
                $post = Post::where('slug',$page)->first();
                if($post)
                    return view('pages.page_template', compact('post'));
                else
                    return view('exceptions.not-found');
        }
//        return response()->json(['success' => isset($products), 'success1' => $request->get('formType'), 'success2' => $request->get('makerType'), 'success3' => $request->get('rating')]);

        if (isset($products) && $request->get('formType') && $request->get('makerType') && $request->get('rating')) {
            $formType = $request->get('formType');
            $makerType = $request->get('makerType');
            $rating = $request->get('rating');

            /**
             * FormTypes filter
             */
            if ($formType !== 'default') {
                $products = $products->whereType($formType);
            }

            /**
             * Maker filter
             */
            if ($makerType !== 'default') {
                $products = $products->whereHas('drug', function ($query) use ($makerType) {
                    $query->whereMaker($makerType);
                });
            }

            /**
             * Sort filter
             */
            if ($rating !== 'default') {
                if ($rating === 'price-cheap') {
                    $products = $products->orderBy('price', 'asc');
                } elseif ($rating === 'price-costly') {
                    $products = $products->orderBy('price', 'desc');
                } elseif ($rating === 'availability') {
                    $products = $products->orderBy('quantity', 'desc');
                }

            }
        }

        if (isset($products)) {
            /**
             * Get Paginated products
             */
            $products = $products->paginate(9);

            if(isset($request->load)){

                $html = view('layouts.list_product_elements')->with(compact('products'))->render();
                $paginator = view('layouts.paginator')->with(compact('products'))->render();


                return response()->json(['success' => true, 'html' => $html, 'paginator' => $paginator]);
            }


            return view('pages.catalog', [
                'products' => $products->appends(Input::except('page')),
                'category' => $category,
                'formType' => $formType,
                'makerType' => $makerType,
                'rating' => $rating
            ]);
        }

        return view('exceptions.not-found');
    }

//    public function ajax_show(Request $request){
//
//        $products = $this->drugs->getQuery();
//
//        if (isset($products)) {
//            /**
//             * Get Paginated products
//             */
//            $products = $products->paginate(9);
//
//            $products = $products->appends(Input::except('page'));
//            $html = view('layouts.list_product_elements')->with(compact('products'))->render();
//
//            $currentPage = $request->page + 1;
//            Paginator::currentPageResolver(function () use ($currentPage) {
//                return $currentPage;
//            });
//
//            $paginator = view('layouts.paginator')->with(compact('products'))->render();
//
//
//            return response()->json(['success' => true, 'html' => $html, 'paginator' => $paginator]);
//        }
//        return null;
//    }
}
