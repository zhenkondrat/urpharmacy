<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Models\DrugPortion;
use App\Models\Order;
use App\Repositories\OrderRepository;
use Gloudemans\Shoppingcart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    protected $cart;
    protected $order;

    /**
     * OrderController constructor.
     * @param Cart $cart
     * @param OrderRepository $order
     */
    public function __construct(Cart $cart, OrderRepository $order)
    {
        $this->cart = $cart;
        $this->order = $order;
    }

    /**
     * @param OrderRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function make(OrderRequest $request)
    {
        $data = [
            'name' => $request->name,
            'phone' => $request->tel,
            'description' => $request->description,
            'total' => 0
        ];

        foreach ($request->items as $id => $item) {
            $portion = DrugPortion::find($id);

            $price = $portion->discount_price ?? $portion->price ?? 0;

            $items[] = [
                'drug_portion_id' => $portion->id,
                'count' => $item['quantity'],
                'amount' => $price
            ];

            $data['total'] += $price * $item['quantity'];
        }

        $message = 'Дякуємо за Ваше замовлення!';

        if (Auth::guest()) {
            $this->order->create($data, $items);

            session()->flash('message', $message);

            return redirect(route('pages.index'));
        }

        $this->order->create($data, $items, Auth::user());

        Session::put('quantity', 0);

        session()->flash('message', $message);

        return redirect(route('pages.index'));
    }
}
