<?php

namespace App\Repositories;

use App\Models\Drug;
use App\Models\DrugPortion;
use App\Models\HotProducts;

/**
 * Class DrugRepository
 *
 * @package App\Repositories
 */
class DrugRepository
{
    /**
     * @param array $data
     *
     * @return Drug|false
     */
    public function create(array $data)
    {
        $drug = new Drug($data);

        return $drug->save() ? $drug : false;
    }

    /**
     * @param $code
     * @return Drug|null
     */
    public function find($code)
    {
        return Drug::whereCode($code)->firstOrFail();
    }

    /**
     * @param Drug $drug
     * @param array $data
     *
     * @return bool
     */
    public function update(Drug $drug, array $data)
    {
        return $drug->update($data);
    }

    /**
     * @param Drug $drug
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function delete(Drug $drug)
    {
        \DB::beginTransaction();

        try {

            if ($drug->delete()) {

                \DB::commit();

                return true;
            }

        } catch (\Exception $e) {
            report($e);
        }

        \DB::rollBack();

        return false;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return DrugPortion::query();
    }

    /**
     * @param $category
     * @return Drug|\Illuminate\Database\Eloquent\Builder
     */
    public function getByCategoryQuery($category)
    {
        return DrugPortion::whereHas('drug', function ($query) use ($category) {
            $query->whereCategoryId($category);
        });
    }

    /**
     * @param $category
     * @return Drug|\Illuminate\Database\Eloquent\Builder
     */
    public function getByCategoryTypeQuery($category)
    {
        return DrugPortion::whereHas('drug', function ($query) use ($category) {
            $query->whereHas('category', function ($query) use ($category) {
                $query->whereType($category);
            });
        });
    }

    /**
     * @param Drug $drug
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAllPortions(Drug $drug)
    {
        return $drug->portions()->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findPortion($id)
    {
        return DrugPortion::findOrFail($id);
    }

    /**
     * Get Actual Products
     *
     * @return mixed
     */
    public function getActualProductsQuery()
    {
        $products = DrugPortion::whereHas('drug', function ($query) {
            $query->whereHas('hotProducts', function ($query) {
                $query->whereType(HotProducts::ACTUAL_PRODUCTS);
            });
        });

        return $products;
    }

    /**
     * Get Popular Products
     *
     * @return mixed
     */
    public function getPopularProductsQuery()
    {
        $products = DrugPortion::whereHas('drug', function ($query) {
            $query->whereHas('hotProducts', function ($query) {
                $query->whereType(HotProducts::POPULAR_PRODUCTS);
            });
        });

        return $products;
    }

    /**
     * Get Discounted Products
     *
     * @return mixed
     */
    public function getDiscountProductsQuery()
    {
        return DrugPortion::whereNotNull('discount_price');
    }

    public function getSearchQuery($word)
    {
        return DrugPortion::whereHas('drug', function($query) use ($word) {
            $query->where('title', 'like', '%' . $word . '%');
        })->OrWhere('title', 'like', '%' . $word . '%');
    }
}
