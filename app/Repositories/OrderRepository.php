<?php

namespace App\Repositories;

use App\Models\{
    OrderItems, User, Order
};
use Gloudemans\Shoppingcart\Cart;

/**
 * Class OrderRepository
 *
 * @package App\Repositories\Order
 */
class OrderRepository
{
    /**
     * @var Cart $cart
     */
    protected $cart;

    /**
     * OrderRepository constructor.
     * @param Cart $cart
     */
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }
    /**
     * @param array $data
     * @param array $items
     * @param User $user
     *
     * @return false|\Illuminate\Database\Eloquent\Model|Order
     */
    public function create(array $data, array $items, User $user = null)
    {
        if ($user) {
            $order = $user->orders()->create($data);
        } else {
            $order = Order::create($data);
        }

        foreach($items as $item) {
            $orderItems[] = new OrderItems($item);
        }

        if ($order->items()->saveMany($orderItems)) {
            $this->cart->destroy();
        }

        return $order ?? false;
    }

    /**
     * @param Order $order
     * @param array $data
     *
     * @return Order|false
     */
    public function update(Order $order, array $data)
    {
        return $order->update($data) ?? false;
    }

    /**
     * @param Order $order
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete(Order $order)
    {
        return $order->delete();
    }

    /**
     * @param User $user
     * @return Order[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getUserOrders(User $user)
    {
        return $user->orders;
    }
}
