<?php

namespace App\Repositories;

use App\Models\AtsCategory;
use App\Models\Category;
use App\Models\Drug;
use App\Models\DrugPortion;

/**
 * Class CategoryRepository
 *
 * @package App\Repositories
 */
class CategoryRepository
{
    /**
     * @param array $data
     *
     * @return Category|false
     */
    public function create(array $data)
    {
        $category = new Category($data);

        return $category->save() ? $category : false;
    }

    /**
     * @param Category $category
     * @param array $data
     *
     * @return bool
     */
    public function update(Category $category, array $data)
    {
        return $category->update($data);
    }

    /**
     * @param Category $category
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function delete(Category $category)
    {
        \DB::beginTransaction();

        try {

            if ($category->delete()) {

                \DB::commit();

                return true;
            }

        } catch (\Exception $e) {
            report($e);
        }

        \DB::rollBack();

        return false;
    }

    /**
     * @param array $filters
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAll(array $filters = [])
    {
        return Category::filter($filters)->paginate();
    }

    /**
     * @param Category $category
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getSubcategories(Category $category)
    {
        return $category->subcategories()->get();
    }

    public function getMainCategories()
    {
        return Category::whereNull('category_id')->get();
    }

    public function getCategoryBySlug($slug)
    {
        return Category::whereSlug($slug)->first();
    }

    public function getAtsSubcategories(AtsCategory $category)
    {
        return $category->subcategories()->get();
    }

    public function getAtsMainCategories()
    {
        return AtsCategory::whereNull('category_id')->get();
    }

    public function getAtsCategoryById($id)
    {
        return AtsCategory::find($id);
    }

    public function getAtsProductsById($id)
    {
        return DrugPortion::whereHas('drug', function($query) use ($id) {
            $query->where('ats_category_id', $id);
        })->orderBy('title');

    }
}
