<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\CategoryType;
use App\Models\Drug;
use App\Models\DrugPortion;
use App\Models\Unit;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\View\View as ViewInstance;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('pages.catalog', function (ViewInstance $view) {
            $formTypes = Unit::getUnits();
            $makerTypes = Drug::pluck('maker', 'maker')->unique();

            $view->with('formTypes', $formTypes);
            $view->with('makerTypes', $makerTypes);
        });

        View::composer(['layouts.nav', 'layouts.aside_nav'], function (ViewInstance $view) {
            $categories = Category::whereNotNull('type')->whereNull('category_id')->get();
            $category_types = CategoryType::get();

            $view->with('categories', $categories)
                ->with('category_types', $category_types);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
