<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTotalFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->double('total')->after('description');
        });

        Schema::table('order_items', function (Blueprint $table) {
            $table->double('amount')->after('count');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('city')->after('phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('total');
        });

        Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn('amount');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('city');
        });
    }
}
