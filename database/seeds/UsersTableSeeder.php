<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = Carbon::create(2015, 5, 28, 0, 0, 0);

        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            $created = $date->addWeeks(rand(1, 20))->format('Y-m-d H:i:s');
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'password' => bcrypt('secret'),
                'created_at' =>  $created,
                'updated_at' =>  Carbon::parse($created)->addWeeks(rand(1, 20))->format('Y-m-d H:i:s')
            ]);
        }
    }
}
